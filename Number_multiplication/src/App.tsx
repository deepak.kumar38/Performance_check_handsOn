import { useState } from "react";

function ListItem({ item }) {
  return <li>{item}</li>;
}

function DataListing() {
  const listData = [];

  for (let i = 1; i < 100; i++) {
    listData.push(<ListItem item={Math.floor(1000 + Math.random() * 9000)} />);
  }

  return <ul>{listData}</ul>;
}

function App() {
  const [data, setData] = useState<number>();

  return (
    <div className="App">
      <div className="">
        <input
          type="number"
          name=""
          id=""
          placeholder="type here..."
          onChange={(e) => {
            setData(e.target.value);
          }}
        />
        {!!data && (
          <p>
            {data} * 2 ={2 * data}
          </p>
        )}
      </div>

      <DataListing />
    </div>
  );
}

export default App;
