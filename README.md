# Performance Check HandsOn

## What is react Virtual DOM

Virtual DOM: React uses Virtual DOM exists which is like a lightweight copy of the actual DOM(a virtual representation of the DOM). So for every object that exists in the original DOM, there is an object for that in React Virtual DOM. It is exactly the same, but it does not have the power to directly change the layout of the document. Manipulating DOM is slow, but manipulating Virtual DOM is fast as nothing gets drawn on the screen. So each time there is a change in the state of our application, the virtual DOM gets updated first instead of the real DOM

## Are we really taking advantage of virtual DOM?

we can take advantege of virtual DOM with Well structured design of DOM and proper use of react Hooks. else it can couse your component re-render, and your app may get slow down in performance.

## How to check re rendering component in DOM?

With help of react Developer tool we can measure performance for your app. in React Dev tool there is an option called React Profiler we ca measure the performance for end user machine.

### Component chart

This is a chart of all the components being profiled in a commit. To understand what a commit is, we have to understand how React renders a component. It does this in two phases —

The render phase where it determines what changes need to be made to e.g. the DOM. During this phase, React calls render and then compares the result to the previous render.
The commit phase is when React applies any changes. (In the case of React DOM, this is when React inserts, updates, and removes DOM nodes)

### Flamegraph tab

Flamegraph tab shows the component tree for the current commit. It Includes render information about each component in the tree. The length and colour of the bars indicates how long the render took.

- The longer a bar is relative to the other bars, the more time it took to render
- The yellower a bar is, the more time it took to render
- The bluer a bar is, the less time it took to render
- If the bar is grey, it didn’t render

### Ranked tab

Ranked tab — Similar to Flamegraph but sorts the components based on which took the longest time to render. The slowest components will be at the top and the fastest will be at the bottom. This makes it easy to identify which components are affecting performance the most.

### Commits overview

This shows an overview of all the commits (renders that led to actual DOM updates) being made as the app renders. The slower a commit is (i.e the slower the render), the longer the bar will be. We can navigate back and forth between commits to get more information about a particular commit.

> Measuring the performance of your application in development mode may give you sugar-coated results.

> In order to get an accurate measurement, you need to profile your application in production mode, as this is what the users would experience.

## How to use the Profiler component

In the simplest of terms, it wraps over a React component and measures how often the component renders and how long the renders take. To use it, you wrap the component you want to measure in the Profiler component —

```
<Profiler id="definition" onRender={callbackToProcessRenderInfo}>
  <Definition />
</Profiler>
```
